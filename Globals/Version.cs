﻿using System;

namespace Globals
{
	public class Version
	{
		public const string APP = "Web App",
		                    VERSION = "1.0.1",
		                    APP_VERSION = APP + " - " + VERSION;
	}
}