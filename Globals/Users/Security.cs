﻿#nullable enable

namespace Globals
{
	public class Users
	{
		public static string CarrierId { get; set; } = "";
		public static string UserId    { get; set; } = "";

		public static class Security
		{
			private static readonly object LockObject = new();

			private static bool _IsAdmin;

			private static bool _IsDriver;

			public static bool IsAdmin
			{
				get
				{
					lock( LockObject )
						return _IsAdmin;
				}
				set
				{
					lock( LockObject )
						_IsAdmin = value;
				}
			}

			public static bool IsDriver
			{
				get
				{
					lock( LockObject )
						return _IsDriver;
				}
				set
				{
					lock( LockObject )
						_IsDriver = value;
				}
			}
		}
	}
}