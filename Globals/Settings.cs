﻿#nullable enable

using System.Threading.Tasks;
using AzureRemoteService;
using Protocol.Data;

namespace Globals.Settings
{
	public class Menu
	{
		private static MenuPreference? _Preferences;

		public static async Task<MenuPreference> Preferences()
		{
			return _Preferences ??= await Azure.Client.RequestMenuPreferences();
		}

		public static void ResetMenuPreferences()
		{
			Task.Run( async () => { _Preferences = await Azure.Client.RequestMenuPreferences(); } );
		}
	}
}