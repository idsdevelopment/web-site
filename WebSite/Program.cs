using System.Threading.Tasks;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.WebAssembly.Hosting;
using Microsoft.Extensions.DependencyInjection;

namespace WebSite
{
	public class Program
	{
		public static async Task Main( string[] args )
		{
			var Builder = WebAssemblyHostBuilder.CreateDefault( args );
			Builder.RootComponents.Add<App>( "#app" );

			var Services = Builder.Services;
			Services.AddBlazoredLocalStorage();
			Services.AddAuthorizationCore();
			Services.AddScoped<ClipboardService>();
			await Builder.Build().RunAsync();
		}
	}
}