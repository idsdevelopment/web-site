﻿#nullable enable

using System.Threading.Tasks;
using Microsoft.JSInterop;

public sealed class ClipboardService
{
	private readonly IJSRuntime JsRuntime;

	public ValueTask<string> ReadTextAsync() => JsRuntime.InvokeAsync<string>( "navigator.clipboard.readText" );

	public ValueTask WriteTextAsync( string text ) => JsRuntime.InvokeVoidAsync( "navigator.clipboard.writeText", text );

	public ClipboardService( IJSRuntime jsRuntime )
	{
		JsRuntime = jsRuntime;
	}
}