﻿var map = null, pin, lineSource = null, pinSource = null;
var animation;
var animationTime = 15000;

var RoutePoints;
var AnimationType;

var Loop = false;
var Reverse = false;
var RotationOffset = 0; // 90
var pipeline;
var routeURL;
var Markers = null;


function InitialiseAzureMap(mapId, centreLatitude, centreLongitude, key) {
    //Initialize a map instance.
    map = new atlas.Map(mapId,
        {
            center: [centreLongitude, centreLatitude],
            zoom: 12,
            language: "en-US",
            authOptions: {
                authType: "subscriptionKey",
                subscriptionKey: key
            }
        });

    pipeline = atlas.service.MapsURL.newPipeline(new atlas.service.MapControlCredential(map));
    routeURL = new atlas.service.RouteURL(pipeline);
}

function BeginAzureRoute() {
    RoutePoints = [];
    if (map != null) {
        Markers = [];
    }
}

function AddAzurePoint(latitude, longitude, dateTimeAsString) {
   // console.log(latitude + ", " + longitude + ", " + dateTimeAsString);

    RoutePoints.push(new atlas.data.Feature(new atlas.data.Point([longitude, latitude]),
        { _timestamp: new Date(dateTimeAsString).getTime() }));
}

function AddAzurePointAAndMarker(latitude, longitude, dateTimeAsString, pinText) {
    AddAzurePoint(latitude, longitude, dateTimeAsString);
    Markers.push([latitude, longitude, pinText]);
}

function SetAzureAnimation(animationType) {
    AnimationType = animationType.toLowerCase;
    switch (AnimationType) {
    case "none":
        Loop = false;
        Reverse = false;
        RotationOffset = 0;
        break;

    default:
        break;
    }
}


function ShowAzureRoute(centreLatitude, centreLongitude) {
    //Load a custom image icon into the map resources.
    map.imageSprite.createFromTemplate("arrow-icon", "marker-arrow", "teal", "#fff").then(function() {

        if (lineSource != null) {
            lineSource.clear();
            pinSource.clear();
        } else {
            lineSource = new atlas.source.DataSource();
            pinSource = new atlas.source.DataSource();
            map.sources.add([lineSource, pinSource]);
        }


        //Create a layer to render the path.
        map.layers.add(new atlas.layer.LineLayer(lineSource,
            null,
            {
                strokeColor: "DodgerBlue",
                strokeWidth: 3
            }));

        //Create a layer to render a symbol which we will animate.
        map.layers.add(new atlas.layer.SymbolLayer(pinSource,
            null,
            {
                iconOptions: {
                    //Pass in the id of the custom icon that was loaded into the map resources.
                    image: "arrow-icon",

                    //Anchor the icon to the center of the image.
                    anchor: "center",

                    //Rotate the icon based on the rotation property on the point data.
                    //The arrow icon being used in this case points down, so we have to rotate it 180 degrees.
                    rotation: ["+", 180, ["get", "heading"]],

                    //Have the rotation align with the map.
                    rotationAlignment: "map",

                    //For smoother animation, ignore the placement of the icon. This skips the label collision calculations and allows the icon to overlap map labels. 
                    ignorePlacement: true,

                    //For smoother animation, allow symbol to overlap all other symbols on the map.
                    allowOverlap: true
                },
                textOptions: {
                    //For smoother animation, ignore the placement of the text. This skips the label collision calculations and allows the text to overlap map labels.
                    ignorePlacement: true,

                    //For smoother animation, allow text to overlap all other symbols on the map.
                    allowOverlap: true
                }
            }));

        //Create a pin and wrap with the shape class and add to data source.
        pin = new atlas.Shape(RoutePoints[0]);
        pinSource.add(pin);

        //Create the animation.
        animation = atlas.animations.moveAlongRoute(RoutePoints,
            pin,
            {
                //Specify the property that contains the timestamp.
                timestampProperty: "timestamp",

                //Capture metadata so that data driven styling can be done.
                captureMetadata: true,

                loop: Loop,
                reverse: Reverse,
                rotationOffset: RotationOffset,

                //Animate such that 1 second of animation time = 1 minute of data time.
                speedMultiplier: 60,

                //If following enabled, add a map to the animation.
                map: map,

                //Camera options to use when following.
                zoom: 5,
                pitch: 45,
                rotate: true
            });

        snapPointsToRoute();
        /*
        const bounds = atlas.data.BoundingBox.fromPositions(path);

        map.setCamera({
            bounds: bounds,
            padding: 20
        });
        */
    });

    function snapPointsToRoute() {
        //Get all the GPS trace data from the datasource as GeoJSON and create an array.
        //  const points = RoutePoints.toJson().features;

        //Extract the Point geometries from the array of features and use them as supporting points in the route request.
        const supportingPoints = RoutePoints.map((val) => {
            return val.geometry;
        });

        //When reconstructing a route, the start and end coordinates must be specified in the query.
        const coordinates = [
            RoutePoints[0].geometry.coordinates, RoutePoints[RoutePoints.length - 1].geometry.coordinates
        ];

        //Pass all coordinates to reconstruct the route and create a logical path as supporting points in the body of the request.
        const options = {
            postBody: {
                "supportingPoints": {
                    "type": "GeometryCollection",
                    "geometries": supportingPoints
                }
            }
        };

        //Calculate a route.
        routeURL.calculateRouteDirections(atlas.service.Aborter.timeout(10000), coordinates, options).then(
            (directions) => {
                //Get the logical route path as GeoJSON and add it to the data source.
                var data = directions.geojson.getFeatures();
                lineSource.clear();
                lineSource.add(data);

                map.markers.clear();

                Markers.forEach(marker => {
                    var [lat, lng, pText] = marker;

                    //Create a HTML marker and add it to the map.
                    map.markers.add(new atlas.HtmlMarker({
                        color: 'DodgerBlue',
                        text: pText,
                        position: [lng, lat]
                    }));
                });

                //Update the map view to center over the route.
                map.setCamera({
                    bounds: data.bbox,
                    padding: 40 //Add a padding to account for the pixel size of symbols.
                });
            });
    }
}