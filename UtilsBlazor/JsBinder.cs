﻿#nullable enable

using System;
using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace UtilsBlazor
{
	public class JsBinder
	{
		public JsBinder( IJSRuntime jsRuntime, string script )
		{
			Script    = script;
			JsRuntime = jsRuntime;

			if( JsRuntime is null )
				Console.WriteLine( "JsRuntime is null" );
		}

		public JsBinder( IJSRuntime jsRuntime ) : this( jsRuntime, "" )
		{
		}

		private readonly IJSRuntime? JsRuntime;

		private Task<IJSObjectReference>? Module;

		protected string Script;

		internal async Task<IJSObjectReference?> GetImportModule()
		{
			if( JsRuntime is not null )
				return await ( Module ??= JsRuntime.InvokeAsync<IJSObjectReference>( "import", Script ).AsTask() );
			return null;
		}

		public async ValueTask DisposeAsync()
		{
			if( Module is not null )
			{
				var M = await Module;
				await M.DisposeAsync();
			}
		}

		public async ValueTask Invoke( string functionName )
		{
			if( JsRuntime is not null )
				await JsRuntime.InvokeVoidAsync( functionName );
		}

		public async ValueTask Invoke( string functionName, object[] args )
		{
			if( JsRuntime is not null )
				await JsRuntime.InvokeVoidAsync( functionName, args );
		}
	}
}