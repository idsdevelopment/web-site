﻿#nullable enable

using System;
using System.Threading.Tasks;
using Microsoft.JSInterop;

namespace UtilsBlazor.GeoLocation
{
	/// <summary>
	///     An implementation of <see cref="IGeolocationService" /> that provides
	///     an interop layer for the device's Geolocation API.
	/// </summary>
	public class GeolocationService : IGeolocationService
	{
		private const string GEOLOCATION_JS = "/js/Geolocation.js";

		/// <summary>
		///     Constructs a <see cref="GeolocationService" /> object.
		/// </summary>
		public GeolocationService( IJSRuntime jsRuntime )
		{
			JsBinder = new JsBinder( jsRuntime, GEOLOCATION_JS );
		}

		private readonly JsBinder JsBinder;

		/// <summary>
		///     Invokes the <see cref="WatchPositionReceived" /> event handler.
		///     Invoked by the success and error callbacks of the JavaScript watchPosition() function.
		/// </summary>
		/// <param name="watchResult">A <see cref="GeolocationResult" /> passed back from JavaScript.</param>
		[JSInvokable]
		public void SetWatchPosition( GeolocationResult watchResult )
		{
			WatchPositionReceived?.Invoke( this, new GeolocationEventArgs
			                                     {
				                                     GeolocationResult = watchResult
			                                     } );
		}

		/// <inheritdoc />
		public event EventHandler<GeolocationEventArgs>? WatchPositionReceived;

		/// <inheritdoc />
		public async Task<GeolocationResult?> GetCurrentPosition( PositionOptions? options = null )
		{
			var Module = await JsBinder.GetImportModule();

			if( Module is not null )
				return await Module.InvokeAsync<GeolocationResult>( "Geolocation.getCurrentPosition", options );
			return null;
		}

		/// <inheritdoc />
		public async Task<long?> WatchPosition( PositionOptions? options = null )
		{
			var Module      = await JsBinder.GetImportModule();
			var CallbackObj = DotNetObjectReference.Create( this );

			if( Module is not null )
			{
				return await Module.InvokeAsync<int>( "Geolocation.watchPosition",
				                                      CallbackObj, nameof( SetWatchPosition ), options );
			}
			return null;
		}

		/// <inheritdoc />
		public async Task ClearWatch( long watchId )
		{
			var Module = await JsBinder.GetImportModule();

			if( Module is not null )
				await Module.InvokeVoidAsync( "Geolocation.clearWatch", watchId );
		}
	}
}