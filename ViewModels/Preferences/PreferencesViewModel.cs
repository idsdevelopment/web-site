﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AzureRemoteService;
using Protocol.Data;
using ViewModelsBase;

namespace ViewModels.Models.Preferences
{
	public class PreferencesViewModel : ViewModelBase
	{
		public class DisplayPreference : Preference
		{
			public new bool Enabled
			{
				get => base.Enabled;
				set
				{
					base.Enabled = value;
					SetCurrent();
				}
			}

			public new string StringValue
			{
				get => base.StringValue;
				set
				{
					base.StringValue = value;
					SetCurrent();
				}
			}

			public new int IntValue
			{
				get => base.IntValue;
				set
				{
					base.IntValue = value;
					SetCurrent();
				}
			}

			public bool BoolValue
			{
				get => base.IntValue > 0;
				set
				{
					base.IntValue = value ? 1 : 0;
					SetCurrent();
				}
			}

			public new DateTimeOffset DateTimeValue
			{
				get => base.DateTimeValue;
				set
				{
					base.DateTimeValue = value;
					SetCurrent();
				}
			}

			public new decimal DecimalValue
			{
				get => base.DecimalValue;
				set
				{
					base.DecimalValue = value;
					SetCurrent();
				}
			}

			public bool ShowBool     => DataType == BOOL;
			public bool ShowString   => DataType == STRING;
			public bool ShowInt      => DataType == INT;
			public bool ShowDecimal  => DataType == DECIMAL;
			public bool ShowDateTime => DataType == DATE_TIME;
			public bool ShowLabel    => DataType == LABEL;

			internal DisplayPreference( PreferencesViewModel owner, Preference preference ) : base( preference )
			{
				Owner = owner;
			}

			//	public FontWeight Bold => IdsOnly ? FontWeights.Bold : FontWeights.Normal;
			private readonly PreferencesViewModel Owner;

			private void SetCurrent()
			{
				Owner.SelectedPreference = null;
				Owner.SelectedPreference = this;
			}
		}

		public class Preferences : List<DisplayPreference>
		{
			public Preferences( PreferencesViewModel owner )
			{
				Owner = owner;
			}

			private readonly PreferencesViewModel Owner;

			public void Add( Preference preference )
			{
				base.Add( new DisplayPreference( Owner, preference ) );
			}
		}

		public Preferences Items
		{
			get { return Get( () => Items, new Preferences( this ) ); }
			set { Set( () => Items, value ); }
		}

		public DisplayPreference? SelectedPreference
		{
			get { return Get( () => SelectedPreference, (DisplayPreference)null! ); }
			set { Set( () => SelectedPreference, value ); }
		}

		public PreferencesViewModel( Action refresh ) : base( refresh )
		{
			Task.Run( async () =>
			          {
				          var Prefs = await Azure.Client.RequestPreferences();

				          var Temp = new Preferences( this );

				          foreach( var Preference in Prefs )
					          Temp.Add( Preference );

				          Items = Temp;
				          Refresh();
			          } );
		}

		[DependsUpon350( nameof( SelectedPreference ) )]
		public async Task WhenSelectedPreferencesChanges()
		{
			if( !( SelectedPreference is null ) )
				await Azure.Client.RequestUpdatePreference( SelectedPreference );
		}
	}
}