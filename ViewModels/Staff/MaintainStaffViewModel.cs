﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzureRemoteService;
using Utils;
using ViewModelsBase;
using Version = Globals.Version;

namespace ViewModels.Models.Staff
{
	public class MaintainStaffViewModel : ViewModelBase
	{
		private const string NEW_NOTE = "New Note";

		public class Role
		{
			public string RoleName { get; set; } = "";
			public bool   Selected { get; set; }
		}

		public class StaffCode
		{
			public string Code      { get; set; } = "";
			public string FirstName { get; set; } = "";
			public string LastName  { get; set; } = "";
		}

		public bool PasswordOk
		{
			get { return Get( () => PasswordOk, true ); }
			set { Set( () => PasswordOk, value ); }
		}

		public string PasswordOkAsString
		{
			get { return Get( () => PasswordOkAsString, "true" ); }
			set { Set( () => PasswordOkAsString, value ); }
		}

		public List<StaffCode> StaffCodes
		{
			get { return Get( () => StaffCodes, new List<StaffCode>() ); }
			set { Set( () => StaffCodes, value ); }
		}


		public string SelectedStaffMember
		{
			get { return Get( () => SelectedStaffMember, "" ); }
			set { Set( () => SelectedStaffMember, value ); }
		}

		public List<Role> StaffRoles
		{
			get { return Get( () => StaffRoles, new List<Role>() ); }
			set { Set( () => StaffRoles, value ); }
		}

		public MaintainStaffViewModel( Action refresh ) : base( refresh )
		{
			Task.Run( () =>
			          {
				          var Tasks = new[]
				                      {
					                      Task.Run( async () =>
					                                {
						                                var Staff = await Azure.Client.RequestGetStaff();
						                                var Codes = new List<StaffCode>();

						                                foreach( var S in Staff )
						                                {
							                                Codes.Add( new StaffCode
							                                           {
								                                           Code      = S.StaffId,
								                                           FirstName = S.FirstName,
								                                           LastName  = S.LastName
							                                           } );
						                                }

						                                StaffCodes     = Codes;
						                                SaveStaffCodes = Codes;
					                                } ),

					                      Task.Run( async () =>
					                                {
						                                var Roles = await Azure.Client.RequestRoles();

						                                var TempRoles = new List<Role>();

						                                foreach( var Role in Roles )
						                                {
							                                TempRoles.Add( new Role
							                                               {
								                                               RoleName = Role.Name
							                                               } );
						                                }

						                                StaffRoles = TempRoles;
					                                } )
				                      };

				          Task.WhenAll( Tasks );
				          Disabled = false;
			          } );
		}

		private readonly string PROGRAM_NAME = $"{Version.APP_VERSION} - Maintain Staff";

		private List<StaffCode> SaveStaffCodes = null!;


		protected override void OnInitialised()
		{
			base.OnInitialised();
			Disabled = true;
		}

		[DependsUpon( nameof( PasswordOk ) )]
		public void WhenPasswordErrorChanges()
		{
			PasswordOkAsString = PasswordOk.ToString();
		}

		protected override void SetDefaultValues()
		{
			base.SetDefaultValues();
			StaffCodes = SaveStaffCodes;
		}

		public void ExecuteRoleChange()
		{
			Modified = true;
		}

	#region Disable Form
		public bool Disabled
		{
			get { return Get( () => Disabled, false ); }
			set { Set( () => Disabled, value ); }
		}


		public string DisabledAsString
		{
			get { return Get( () => DisabledAsString, "False" ); }
			set { Set( () => DisabledAsString, value ); }
		}


		[DependsUpon( nameof( Disabled ) )]
		public void WhenDisabledChanges()
		{
			DisabledAsString = Disabled.ToString();
		}
	#endregion

	#region Toolbar
		public bool DisableAdd
		{
			get { return Get( () => DisableAdd, false ); }
			set { Set( () => DisableAdd, value ); }
		}


		public string DisableAddAsString
		{
			get { return Get( () => DisableAddAsString, "false" ); }
			set { Set( () => DisableAddAsString, value ); }
		}


		public bool DisableCancel
		{
			get { return Get( () => DisableCancel, true ); }
			set { Set( () => DisableCancel, value ); }
		}


		public string DisableCancelAsString
		{
			get { return Get( () => DisableCancelAsString, "true" ); }
			set { Set( () => DisableCancelAsString, value ); }
		}


		public bool DisableOk
		{
			get { return Get( () => DisableOk, true ); }
			set { Set( () => DisableOk, value ); }
		}


		public string DisableOkAsString
		{
			get { return Get( () => DisableOkAsString, "true" ); }
			set { Set( () => DisableOkAsString, value ); }
		}

		[DependsUpon( nameof( DisableAdd ) )]
		public void WhenAllowAddChanges()
		{
			DisableAddAsString = DisableAdd.ToString();
		}

		[DependsUpon( nameof( DisableCancel ) )]
		public void WhenDisableCancelChanges()
		{
			DisableCancelAsString = DisableCancel.ToString();
		}

		[DependsUpon( nameof( DisableOk ) )]
		public void WhenDisableOkChanges()
		{
			DisableOkAsString = DisableOk.ToString();
		}

		private bool InAdd;

		public void ExecuteCancel()
		{
			if( !InAdd )
			{
				if( SelectedStaffMember.IsNotNullOrWhiteSpace() )
					WhenSelectedStaffMemberChanges();
			}
			else
				SetDefaultValues();

			InAdd         = false;
			DisableCancel = true;
			DisableOk     = true;
			DisableAdd    = false;
			Disabled      = false;
		}

		public void ExecuteAdd()
		{
			foreach( var Role in StaffRoles )
				Role.Selected = false;

			SetDefaultValues();
			StaffCodes    = new List<StaffCode>();
			DisableAdd    = true;
			InAdd         = true;
			DisableCancel = false;
			Disabled      = false;
		}
	#endregion

	#region Staff Member
		public bool Modified
		{
			get { return Get( () => Modified, false ); }
			set { Set( () => Modified, value ); }
		}

		[DependsUpon250( nameof( Enabled ) )]
		[DependsUpon250( nameof( Password ) )]
		[DependsUpon250( nameof( PasswordOk ) )]
		[DependsUpon250( nameof( FirstName ) )]
		[DependsUpon250( nameof( LastName ) )]
		[DependsUpon250( nameof( MiddleNames ) )]
		[DependsUpon250( nameof( Phone ) )]
		[DependsUpon250( nameof( Mobile ) )]
		[DependsUpon250( nameof( Email ) )]
		[DependsUpon250( nameof( Suite ) )]
		[DependsUpon250( nameof( Street ) )]
		[DependsUpon250( nameof( Street1 ) )]
		[DependsUpon250( nameof( City ) )]
		[DependsUpon250( nameof( Region ) )]
		[DependsUpon250( nameof( Country ) )]
		[DependsUpon250( nameof( PostalCode ) )]

		//
		[DependsUpon250( nameof( EmFirstName ) )]
		[DependsUpon250( nameof( EmLastName ) )]
		[DependsUpon250( nameof( EmPhone ) )]
		[DependsUpon250( nameof( EmMobile ) )]
		[DependsUpon250( nameof( EmEmail ) )]

		//
		[DependsUpon250( nameof( MainCommission1 ) )]
		[DependsUpon250( nameof( MainCommission2 ) )]
		[DependsUpon250( nameof( MainCommission3 ) )]

		//
		[DependsUpon250( nameof( CommissionOverride1 ) )]
		[DependsUpon250( nameof( CommissionOverride2 ) )]
		[DependsUpon250( nameof( CommissionOverride3 ) )]
		[DependsUpon250( nameof( CommissionOverride4 ) )]
		[DependsUpon250( nameof( CommissionOverride5 ) )]
		[DependsUpon250( nameof( CommissionOverride6 ) )]
		[DependsUpon250( nameof( CommissionOverride7 ) )]
		[DependsUpon250( nameof( CommissionOverride8 ) )]
		[DependsUpon250( nameof( CommissionOverride9 ) )]
		[DependsUpon250( nameof( CommissionOverride10 ) )]
		[DependsUpon250( nameof( CommissionOverride11 ) )]
		[DependsUpon250( nameof( CommissionOverride12 ) )]
		public void WhenStaffMemberChanges()
		{
			//Toggle Modified
			Modified = false;
			Modified = true;
		}

		[DependsUpon( nameof( Modified ) )]
		public void WhenModifiedChanges()
		{
			var M = Modified;
			DisableAdd    = M;
			DisableCancel = !M;

			var Ok = M && PasswordOk && SelectedStaffMember.IsNotNullOrWhiteSpace();
			DisableOk = !Ok;
		}

		[DependsUpon150( nameof( SelectedStaffMember ) )]
		public void WhenSelectedStaffMemberChanges()
		{
			var StaffId = SelectedStaffMember;

			if( StaffId.IsNotNullOrWhiteSpace() )
			{
				Disabled = true;

				var Tasks = new[]
				            {
					            Task.Run( async () =>
					                      {
						                      DisableEvents = true;

						                      try
						                      {
							                      var S = await Azure.Client.RequestGetStaffMember( StaffId );

							                      if( S.Ok )
							                      {
								                      Enabled     = S.Enabled;
								                      FirstName   = S.FirstName;
								                      LastName    = S.LastName;
								                      MiddleNames = S.MiddleNames;
								                      EmFirstName = S.EmergencyFirstName;
								                      EmLastName  = S.EmergencyLastName;
								                      EmPhone     = S.EmergencyPhone;
								                      EmMobile    = S.EmergencyMobile;
								                      EmEmail     = S.EmergencyEmail;

								                      MainCommission1 = S.MainCommission1;

								                      var A = S.Address;
								                      Phone  = A.Phone;
								                      Mobile = A.Mobile;
								                      Email  = A.EmailAddress;

								                      Suite      = A.Suite;
								                      Street     = A.AddressLine1;
								                      Street1    = A.AddressLine2;
								                      City       = A.City;
								                      Region     = A.Region;
								                      Country    = A.Country;
								                      PostalCode = A.PostalCode;

								                      var (Value, Ok) = Encryption.FromTimeLimitedToken( S.Password );

								                      Password = Ok ? Value : "Error in password";

								                      DisableNotes    = false;
								                      DisableNotesAdd = false;
							                      }
						                      }
						                      finally
						                      {
							                      DisableEvents = false;
						                      }
					                      } ),
					            Task.Run( async () =>
					                      {
						                      var Roles = await Azure.Client.RequestStaffMemberRoles( StaffId );

						                      foreach( var Role in StaffRoles )
						                      {
							                      Role.Selected = false;

							                      var Name = Role.RoleName;

							                      foreach( var R in Roles )
							                      {
								                      if( Name == R.Name )
								                      {
									                      Role.Selected = true;

									                      break;
								                      }
							                      }
						                      }
					                      } ),
					            Task.Run( async () =>
					                      {
						                      var Nts = await Azure.Client.RequestGetStaffNotes( StaffId );

						                      Notes = ( from N in Nts
						                                select N ).ToDictionary( note => note.NoteId, note => note.Text );
					                      } )
				            };

				Task.WaitAll( Tasks );

				Disabled = false;
			}
			else
				SetDefaultValues();
		}


		public string FirstName
		{
			get { return Get( () => FirstName, "" ); }
			set { Set( () => FirstName, value ); }
		}


		public string LastName
		{
			get { return Get( () => LastName, "" ); }
			set { Set( () => LastName, value ); }
		}


		public string MiddleNames
		{
			get { return Get( () => MiddleNames, "" ); }
			set { Set( () => MiddleNames, value ); }
		}


		public string Phone
		{
			get { return Get( () => Phone, "" ); }
			set { Set( () => Phone, value ); }
		}


		public string Mobile
		{
			get { return Get( () => Mobile, "" ); }
			set { Set( () => Mobile, value ); }
		}


		public string Email
		{
			get { return Get( () => Email, "" ); }
			set { Set( () => Email, value ); }
		}


		public string Suite
		{
			get { return Get( () => Suite, "" ); }
			set { Set( () => Suite, value ); }
		}


		public string Street
		{
			get { return Get( () => Street, "" ); }
			set { Set( () => Street, value ); }
		}


		public string Street1
		{
			get { return Get( () => Street1, "" ); }
			set { Set( () => Street1, value ); }
		}


		public string City
		{
			get { return Get( () => City, "" ); }
			set { Set( () => City, value ); }
		}


		public string Region
		{
			get { return Get( () => Region, "" ); }
			set { Set( () => Region, value ); }
		}


		public string Country
		{
			get { return Get( () => Country, "" ); }
			set { Set( () => Country, value ); }
		}


		public string PostalCode
		{
			get { return Get( () => PostalCode, "" ); }
			set { Set( () => PostalCode, value ); }
		}

		public bool Enabled
		{
			get { return Get( () => Enabled, false ); }
			set { Set( () => Enabled, value ); }
		}


		public string Password
		{
			get { return Get( () => Password, "" ); }
			set { Set( () => Password, value ); }
		}

		[DependsUpon350( nameof( Password ) )]
		public void WhenPasswordChanges()
		{
			Task.Run( async () => { PasswordOk = await Azure.Client.RequestIsPasswordValid( Encryption.ToTimeLimitedToken( Password ) ); } );
		}

	#region Emergency
		public string EmFirstName
		{
			get { return Get( () => EmFirstName, "" ); }
			set { Set( () => EmFirstName, value ); }
		}

		public string EmLastName
		{
			get { return Get( () => EmLastName, "" ); }
			set { Set( () => EmLastName, value ); }
		}


		public string EmPhone
		{
			get { return Get( () => EmPhone, "" ); }
			set { Set( () => EmPhone, value ); }
		}


		public string EmMobile
		{
			get { return Get( () => EmMobile, "" ); }
			set { Set( () => EmMobile, value ); }
		}


		public string EmEmail
		{
			get { return Get( () => EmEmail, "" ); }
			set { Set( () => EmEmail, value ); }
		}
	#endregion

	#region Commission
		public decimal MainCommission1
		{
			get { return Get( () => MainCommission1, 0 ); }
			set { Set( () => MainCommission1, value ); }
		}

		public decimal MainCommission2
		{
			get { return Get( () => MainCommission2, 0 ); }
			set { Set( () => MainCommission2, value ); }
		}


		public decimal MainCommission3
		{
			get { return Get( () => MainCommission3, 0 ); }
			set { Set( () => MainCommission3, value ); }
		}


		public decimal CommissionOverride1
		{
			get { return Get( () => CommissionOverride1, 0 ); }
			set { Set( () => CommissionOverride1, value ); }
		}

		public decimal CommissionOverride2
		{
			get { return Get( () => CommissionOverride2, 0 ); }
			set { Set( () => CommissionOverride2, value ); }
		}

		public decimal CommissionOverride3
		{
			get { return Get( () => CommissionOverride3, 0 ); }
			set { Set( () => CommissionOverride3, value ); }
		}

		public decimal CommissionOverride4
		{
			get { return Get( () => CommissionOverride4, 0 ); }
			set { Set( () => CommissionOverride4, value ); }
		}

		public decimal CommissionOverride5
		{
			get { return Get( () => CommissionOverride5, 0 ); }
			set { Set( () => CommissionOverride5, value ); }
		}

		public decimal CommissionOverride6
		{
			get { return Get( () => CommissionOverride6, 0 ); }
			set { Set( () => CommissionOverride6, value ); }
		}

		public decimal CommissionOverride7
		{
			get { return Get( () => CommissionOverride7, 0 ); }
			set { Set( () => CommissionOverride7, value ); }
		}

		public decimal CommissionOverride8
		{
			get { return Get( () => CommissionOverride8, 0 ); }
			set { Set( () => CommissionOverride8, value ); }
		}

		public decimal CommissionOverride9
		{
			get { return Get( () => CommissionOverride9, 0 ); }
			set { Set( () => CommissionOverride9, value ); }
		}

		public decimal CommissionOverride10
		{
			get { return Get( () => CommissionOverride10, 0 ); }
			set { Set( () => CommissionOverride10, value ); }
		}


		public decimal CommissionOverride11
		{
			get { return Get( () => CommissionOverride11, 0 ); }
			set { Set( () => CommissionOverride11, value ); }
		}


		public decimal CommissionOverride12
		{
			get { return Get( () => CommissionOverride12, 0 ); }
			set { Set( () => CommissionOverride12, value ); }
		}
	#endregion
	#endregion


	#region Motes
		public Dictionary<string, string> Notes
		{
			get { return Get( () => Notes, new Dictionary<string, string>() ); }
			set { Set( () => Notes, value ); }
		}

		private Dictionary<string, string> SaveNotes = new();


		[DependsUpon( nameof( Notes ) )]
		public void WhenNotesChange()
		{
			SaveNotes         = new Dictionary<string, string>( Notes );
			DisableNoteDelete = true;
		}

		public bool DisableNotes
		{
			get { return Get( () => DisableNotes, true ); }
			set { Set( () => DisableNotes, value ); }
		}


		public string DisableNotesAsString
		{
			get { return Get( () => DisableNotesAsString, "true" ); }
			set { Set( () => DisableNotesAsString, value ); }
		}

		[DependsUpon( nameof( DisableNotes ) )]
		[DependsUpon( nameof( Disabled ) )]
		public void WenDisableNotesChanges()
		{
			DisableNotesAsString = ( Disabled || DisableNotes ).ToString();
		}


		public bool DisableNotesAdd
		{
			get { return Get( () => DisableNotesAdd, true ); }
			set { Set( () => DisableNotesAdd, value ); }
		}


		public string DisableNotesAddAsString
		{
			get { return Get( () => DisableNotesAddAsString, "true" ); }
			set { Set( () => DisableNotesAddAsString, value ); }
		}


		public bool DisableNotesCancel
		{
			get { return Get( () => DisableNotesCancel, true ); }
			set { Set( () => DisableNotesCancel, value ); }
		}


		public string DisableNotesCancelAsString
		{
			get { return Get( () => DisableNotesCancelAsString, "true" ); }
			set { Set( () => DisableNotesCancelAsString, value ); }
		}

		[DependsUpon( nameof( DisableNotesCancel ) )]
		[DependsUpon( nameof( DisableNotes ) )]
		[DependsUpon( nameof( Disabled ) )]
		public void WhenDisableNotesCancelChanges()
		{
			DisableNotesCancelAsString = ( Disabled || DisableNotes || DisableNotesCancel ).ToString();
		}


		[DependsUpon( nameof( DisableNotesAdd ) )]
		[DependsUpon( nameof( DisableNotes ) )]
		[DependsUpon( nameof( Disabled ) )]
		public void WhenDisableNotesAddChanges()
		{
			DisableNotesAddAsString = ( Disabled || DisableNotesAdd || DisableNotes ).ToString();
		}


		public string NoteText
		{
			get { return Get( () => NoteText, "" ); }
			set { Set( () => NoteText, value ); }
		}

		[DependsUpon150( nameof( NoteText ) )]
		public void WhenNoteTextChanges()
		{
			DisableNotesOk     = false;
			DisableNoteDelete  = true;
			DisableNotesCancel = false;
			DisableNotesOk     = false;
		}

		public string NoteName
		{
			get { return Get( () => NoteName, "" ); }
			set { Set( () => NoteName, value ); }
		}


		[DependsUpon( nameof( NoteName ) )]
		public void WhenNoteNameChanges()
		{
			if( Notes.TryGetValue( NoteName, out var NValue ) )
				NoteText = NValue;

			DisableNotesCancel = false;
			DisableNotesOk     = false;
		}


		public void OnNoteNameChange( string oldName, string newName )
		{
			SelectedNote = oldName;
			NoteName     = newName;
			var Enab = oldName == newName;
			DisableNotesOk     &= Enab;
			DisableNotesCancel &= Enab;
		}

		public void ExecuteNotesAdd()
		{
			SaveNotes = new Dictionary<string, string>( Notes );

			var Cnt = 0;

			foreach( var Note in Notes )
			{
				if( Note.Key.StartsWith( NEW_NOTE, StringComparison.OrdinalIgnoreCase ) )
					Cnt++;
			}

			var Append = Cnt > 0 ? $" - {Cnt}" : "";

			var Name = $"New Note{Append}";
			NoteName = Name;
			Notes.Add( Name, "" );
			DisableNotesAdd    = true;
			DisableNotesCancel = false;
			DisableNotesOk     = false;
			DisableNoteDelete  = true;
		}

		public void ExecuteNotesCancel()
		{
			DisableNotesAdd    = false;
			DisableNotesCancel = true;
			DisableNotesOk     = true;
			DisableNoteDelete  = true;

			Notes    = SaveNotes;
			NoteText = "";
		}


		public bool DisableNotesOk
		{
			get { return Get( () => DisableNotesOk, true ); }
			set { Set( () => DisableNotesOk, value ); }
		}


		public string DisableNotesOkAsString
		{
			get { return Get( () => DisableNotesOkAsString, "true" ); }
			set { Set( () => DisableNotesOkAsString, value ); }
		}

		[DependsUpon( nameof( DisableNotesOk ) )]
		[DependsUpon( nameof( DisableNotes ) )]
		[DependsUpon( nameof( Disabled ) )]
		public void WhenDisableNotesOkAsStringChanges()
		{
			DisableNotesOkAsString = ( Disabled || DisableNotes || DisableNotesOk ).ToString();
		}

		public async void ExecuteNotesOk()
		{
			if( SelectedNote.IsNotNullOrWhiteSpace() && ( SelectedNote != NoteName ) )
			{
				await Azure.Client.RequestRenameStaffNote( PROGRAM_NAME, SelectedStaffMember, SelectedNote, NoteName );
				SelectedNote = NoteName;
			}

			await Azure.Client.RequestAddUpdateStaffNote( PROGRAM_NAME, SelectedStaffMember, NoteName, NoteText );
			ExecuteNotesCancel();
		}

		public bool DisableNoteDelete
		{
			get { return Get( () => DisableNoteDelete, true ); }
			set { Set( () => DisableNoteDelete, value ); }
		}


		public string DisableNoteDeleteAsString
		{
			get { return Get( () => DisableNoteDeleteAsString, "true" ); }
			set { Set( () => DisableNoteDeleteAsString, value ); }
		}

		[DependsUpon( nameof( DisableNoteDelete ) )]
		[DependsUpon( nameof( DisableNotes ) )]
		[DependsUpon( nameof( Disabled ) )]
		public void WhenDisableNoteDeleteChanges()
		{
			DisableNoteDeleteAsString = ( DisableNoteDelete || DisableNotes || Disabled ).ToString();
		}

		private string SelectedNote = null!;

		public void OnNoteEnter( string noteName )
		{
			SelectedNote      = noteName;
			DisableNoteDelete = false;
		}

		public void DeleteNote()
		{
			if( Notes.ContainsKey( SelectedNote ) )
			{
				Notes.Remove( SelectedNote );

				Task.Run( async () => { await Azure.Client.RequestDeleteStaffNote( PROGRAM_NAME, SelectedStaffMember, SelectedNote ); } );
			}

			DisableNoteDelete = true;
		}
	#endregion
	}
}