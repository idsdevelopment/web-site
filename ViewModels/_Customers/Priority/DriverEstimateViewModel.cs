﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzureRemoteService;
using Globals;
using Microsoft.AspNetCore.Components;
using Protocol.Data;
using Protocol.Data.Customers.Priority;
using Utils;
using ViewModels.Components;
using ViewModelsBase;
using Route = Protocol.Data.Maps.Route.Route;

namespace ViewModels._Customers.Priority
{
	public static class Extensions
	{
		public static string ToVisibility( this bool visible ) => visible ? "visible" : "collapse";
	}

	public class DisplayCommission
	{
		public DateTime Date   { get; set; } = DateTime.MinValue;
		public string   TripId { get; set; } = "";

		public string DestinationCompany { get; set; } = "";
		public string DestinationStreet  { get; set; } = "";
		public string DestinationCity    { get; set; } = "";

		public decimal Distance { get; set; }
		public decimal Estimate { get; set; }

		public string ManifestName { get; set; } = "";
		public int    LegNumber    { get; set; }

		public Route? Route;
	}

	public class DisplayGroup
	{
		public string                  Group              { get; set; } = "";
		public List<DisplayCommission> DisplayCommissions { get; set; } = new();
		public decimal                 SubTotalDistance   { get; set; }
		public decimal                 SubTotalEstimate   { get; set; }
		public string                  ServiceLevel       { get; set; } = "";
	}

	public class DisplayGroupCommissions : List<DisplayGroup>
	{
	}

	public class DriverEstimateViewModel : ViewModelBase
	{
		public DriverEstimateViewModel( Action refresh ) : base( refresh )
		{
			var Date = DateTimeOffset.Now.AddDays( -3 );
			ToDate   = Date;
			FromDate = Date.AddDays( -7 );
		}

	#region Visibility
		public string TotalsVisibility
		{
			get { return Get( () => TotalsVisibility, "collapse" ); }
			set { Set( () => TotalsVisibility, value ); }
		}

		public bool TotalsVisible
		{
			get { return Get( () => TotalsVisible, false ); }
			set { Set( () => TotalsVisible, value ); }
		}

		[DependsUpon( nameof( TotalsVisible ) )]
		public void WhenTotalsVisibleChanges()
		{
			TotalsVisibility = TotalsVisible.ToVisibility();
		}


		public string EstimatesVisibility
		{
			get { return Get( () => EstimatesVisibility, "collapse" ); }
			set { Set( () => EstimatesVisibility, value ); }
		}


		public bool EstimatesVisible
		{
			get { return Get( () => EstimatesVisible, false ); }
			set { Set( () => EstimatesVisible, value ); }
		}

		[DependsUpon( nameof( EstimatesVisible ) )]
		public void WhenEstimatesVisibleChanges()
		{
			EstimatesVisibility = EstimatesVisible.ToVisibility();
		}
	#endregion

	#region Route
		public List<MarkupString> Directions
		{
			get { return Get( () => Directions, new List<MarkupString>() ); }
			set { Set( () => Directions, value ); }
		}

		public string RouteName
		{
			get { return Get( () => RouteName, "Route" ); }
			set { Set( () => RouteName, value ); }
		}

		public List<RouteEntry> Route
		{
			get { return Get( () => Route, new List<RouteEntry>() ); }
			set { Set( () => Route, value ); }
		}

		private static async Task LoadRoute( DisplayCommission c )
		{
			if( ( c.Distance > 0 ) && c.Route is null )
			{
				var Temp = await Azure.Client.RequestGetRouteManifest( new RouteManifestRequest
				                                                       {
					                                                       ManifestName = c.ManifestName,
					                                                       LegNumber    = c.LegNumber
				                                                       } );

				c.Route = Temp.Route;
			}
		}

		public async void OnShowRoute( string groupNumber )
		{
			Console.WriteLine( $"Group Number: {groupNumber}" );

			RouteName = $"Route: {groupNumber}";

			var Result = new List<RouteEntry>();

			var Commission = ( from D in DisplayGroupCommissions
			                   where D.Group == groupNumber
			                   select new { D.DisplayCommissions, D.ServiceLevel } ).FirstOrDefault();

			if( Commission is not null )
			{
				foreach( var DisplayCommission in Commission.DisplayCommissions )
					await LoadRoute( DisplayCommission );
			}

			var IsSweep = string.Compare( Commission?.ServiceLevel.Trim(), "SWEEP", StringComparison.OrdinalIgnoreCase ) == 0;

			var Time = DateTimeOffset.Now;

			var Dir = new List<MarkupString>();

			var Route1 = ( from C in Commission?.DisplayCommissions
			               where C.Distance != 0
			               select C.Route ).ToList();

			var PinNo = 0;

			foreach( var R in Route1 )
			{
				if( R is not null )
				{
					var Seconds = 0;

					foreach( var Leg in R )
					{
						if( Leg.Distance != 0 )
						{
							var NewLeg = true;

							foreach( var Step in Leg.Steps )
							{
								Dir.Add( new MarkupString( Step.HtmlInstructions ) );

								if( NewLeg )
								{
									Result.Add( new RouteEntry
									            {
										            DateTime  = Time,
										            Latitude  = Maths.Round6( Step.StartLocation.Latitude ),
										            Longitude = Maths.Round6( Step.StartLocation.Longitude ),
										            AddPin    = NewLeg,
										            PinText   = NewLeg ? PinNo == 0 ? IsSweep ? "B" : "S" : PinNo.ToString() : ""
									            } );
									++PinNo;
								}
								Time = Time.AddMinutes( (int)Step.Duration ).AddSeconds( ++Seconds );

								Result.Add( new RouteEntry
								            {
									            DateTime  = Time,
									            Latitude  = Maths.Round6( Step.EndLocation.Latitude ),
									            Longitude = Maths.Round6( Step.EndLocation.Longitude ),
									            Distance  = Step.Distance
								            } );

								NewLeg = false;
							}
						}
					}
				}
			}
			var Cnt = Result.Count;

			if( !IsSweep && ( Cnt > 0 ) )
			{
				var R = Result[ Cnt - 1 ];
				R.AddPin  = true;
				R.PinText = PinNo.ToString();
			}
			Directions = Dir;
			Route      = Result;
		}
	#endregion

	#region Commissions
		public DisplayGroupCommissions DisplayGroupCommissions
		{
			get { return Get( () => DisplayGroupCommissions, new DisplayGroupCommissions() ); }
			set { Set( () => DisplayGroupCommissions, value ); }
		}


		public decimal TotalDistance
		{
			get { return Get( () => TotalDistance, 0 ); }
			set { Set( () => TotalDistance, value ); }
		}


		public decimal TotalEstimate
		{
			get { return Get( () => TotalEstimate, 0 ); }
			set { Set( () => TotalEstimate, value ); }
		}
	#endregion

	#region Input
		public DateTimeOffset FromDate
		{
			get { return Get( () => FromDate, DateTimeOffset.Now ); }
			set { Set( () => FromDate, value ); }
		}


		public DateTimeOffset ToDate
		{
			get { return Get( () => ToDate, DateTimeOffset.Now ); }
			set { Set( () => ToDate, value ); }
		}

		public string DisableDateSearchAsString
		{
			get { return Get( () => DisableDateSearchAsString, ViewModels.Extensions.FALSE ); }
			set { Set( () => DisableDateSearchAsString, value ); }
		}

		public string DisableCancelAsString
		{
			get { return Get( () => DisableCancelAsString, ViewModels.Extensions.TRUE ); }
			set { Set( () => DisableCancelAsString, value ); }
		}

		public string DisableSearchAsString
		{
			get { return Get( () => DisableSearchAsString, ViewModels.Extensions.FALSE ); }
			set { Set( () => DisableSearchAsString, value ); }
		}
	#endregion

	#region Actions
		public void OnCancel()
		{
		}

		public async void OnSearch()
		{
			var Commissions = await Azure.Client.RequestPriorityGetDriverCommission( new PriorityDriverCommissionRange
			                                                                         {
				                                                                         Driver   = Users.UserId,
				                                                                         FromDate = FromDate,
				                                                                         ToDate   = ToDate
			                                                                         } );

			var Grouped = ( from C in Commissions
			                orderby C.Reference
			                group C by C.Reference
			                into G
			                select new { Group = G.Key, Commissions = G.ToList() } ).ToList();

			var DisplayGroups = new DisplayGroupCommissions();

			decimal TotDistance = 0,
			        TotEstimate = 0;

			foreach( var G in Grouped )
			{
				var First        = true;
				var ServiceLevel = "";

				var DisplayCommissions = new List<DisplayCommission>();

				foreach( var Commission in G.Commissions )
				{
					if( First )
					{
						First        = false;
						ServiceLevel = Commission.ServiceLevel;
					}

					DisplayCommissions.Add( new DisplayCommission
					                        {
						                        Date   = Commission.DateTime.DateTime,
						                        TripId = Commission.TripId,

						                        DestinationCompany = Commission.DeliveryAddress.CompanyName,
						                        DestinationStreet  = Commission.DeliveryAddress.Street,
						                        DestinationCity    = Commission.DeliveryAddress.City,

						                        Distance = Math.Round( new Measurement
						                                               {
							                                               KiloMetres = Commission.Distance
						                                               }.Miles, 1, MidpointRounding.AwayFromZero ),

						                        Estimate = Math.Round( Commission.Commission, 2, MidpointRounding.AwayFromZero ),

						                        ManifestName = Commission.ManifestName,
						                        LegNumber    = Commission.LegNumber
					                        } );
				}

				decimal Distance = 0,
				        Estimate = 0;

				foreach( var Commission in DisplayCommissions )
				{
					Distance += Commission.Distance;
					Estimate += Commission.Estimate;
				}

				TotDistance += Distance;
				TotEstimate += Estimate;

				DisplayGroups.Add( new DisplayGroup
				                   {
					                   Group              = G.Group,
					                   DisplayCommissions = DisplayCommissions,
					                   SubTotalDistance   = Distance,
					                   SubTotalEstimate   = Estimate,
					                   ServiceLevel       = ServiceLevel
				                   } );
			}

			TotalDistance           = TotDistance;
			TotalEstimate           = TotEstimate;
			DisplayGroupCommissions = DisplayGroups;
			TotalsVisible           = Grouped.Count > 0;
			EstimatesVisible        = true;
		}
	#endregion
	}
}