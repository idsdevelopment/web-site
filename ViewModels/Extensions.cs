﻿namespace ViewModels
{
	public static class Extensions
	{
		public const string FALSE = "false",
		                    TRUE  = "true";


		public static string ToVisibility( this bool vis ) => !vis ? "hidden" : "visible";
		public static string ToDisabled( this bool disabled ) => disabled ? TRUE : FALSE;
		public static string ToEnabled( this bool enabled ) => enabled ? FALSE : TRUE;
	}
}