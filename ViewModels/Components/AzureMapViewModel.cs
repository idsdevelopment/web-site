﻿#nullable enable

using System;
using System.Collections.Generic;
using AzureRemoteService;
using Microsoft.JSInterop;
using Utils;
using UtilsBlazor;
using UtilsBlazor.GeoLocation;
using ViewModelsBase;

namespace ViewModels.Components
{
	public class RouteEntry
	{
		public decimal        Latitude  { get; set; }
		public decimal        Longitude { get; set; }
		public DateTimeOffset DateTime  { get; set; }
		public decimal        Distance  { get; set; }
		public bool           AddPin    { get; set; }
		public string         PinText   { get; set; } = "";
	}

	public class AzureMapViewModel : ViewModelBase
	{
		private static string? ApiKey;

		public decimal Latitude
		{
			get { return Get( () => Latitude, 0 ); }
			set { Set( () => Latitude, value ); }
		}


		public decimal Longitude
		{
			get { return Get( () => Longitude, 0 ); }
			set { Set( () => Longitude, value ); }
		}

		public AzureMapViewModel( Action refresh ) : base( refresh )
		{
		}

	#region Route
		public IEnumerable<RouteEntry>? Route
		{
			get { return Get( () => Route, (IEnumerable<RouteEntry>?)null ); }
			set { Set( () => Route, value ); }
		}


		[DependsUpon( nameof( Route ) )]
		public async void WhenRouteChanges()
		{
			if( Route is not null )
			{
				await JsBinder.Invoke( "BeginAzureRoute" );

				var         First = true;
				RouteEntry? R     = null;

				foreach( var RouteEntry in Route )
				{
					if( First )
					{
						First = false;
						R     = RouteEntry;
					}

					var Lat  = Maths.Round6( RouteEntry.Latitude );
					var Lng  = Maths.Round6( RouteEntry.Longitude );
					var Date = $"{RouteEntry.DateTime:G}";

					Console.WriteLine( $"--> {Lat}, {Lng}, {Date}, {RouteEntry.PinText}" );

					if( !RouteEntry.AddPin )
						await JsBinder.Invoke( "AddAzurePoint", new object[] { (double)Lat, (double)Lng, Date } );
					else
						await JsBinder.Invoke( "AddAzurePointAAndMarker", new object[] { (double)Lat, (double)Lng, Date, RouteEntry.PinText } );
				}
				await JsBinder.Invoke( "SetAzureAnimation", new object[] { Animation.ToLower() } );

				if( R is not null )
					await JsBinder.Invoke( "ShowAzureRoute", new object[] { Maths.Round6AsDouble( R!.Latitude ), Maths.Round6AsDouble( R.Longitude ) } );
			}
		}
	#endregion

	#region Options
		public string Header
		{
			get { return Get( () => Header, "" ); }
			set { Set( () => Header, value ); }
		}


		public string HeaderClass
		{
			get { return Get( () => HeaderClass, "" ); }
			set { Set( () => HeaderClass, value ); }
		}


		public string Class
		{
			get { return Get( () => Class, "" ); }
			set { Set( () => Class, value ); }
		}


		public string Animation
		{
			get { return Get( () => Animation, "" ); }
			set { Set( () => Animation, value ); }
		}
	#endregion

	#region MapId
		public string MapId
		{
			get { return Get( () => MapId, "" ); }
			set { Set( () => MapId, value ); }
		}

		[DependsUpon( nameof( MapId ) )]
		public async void WhenMapIdChanges()
		{
			ApiKey ??= ( await Azure.Client.RequestGetMapsApiKeys() ).AzureMapKey;

			var Service = new GeolocationService( JsRuntime );
			var Pos     = await Service.GetCurrentPosition();

			if( Pos is not null )
			{
				var Position = Pos.Position;

				var Coords = Position?.Coords;

				if( Coords is not null )
				{
					Latitude  = (decimal)Coords.Latitude;
					Longitude = (decimal)Coords.Longitude;
				}
			}
			await JsBinder.Invoke( "InitialiseAzureMap", new object[] { MapId, (double)Latitude, (double)Longitude, ApiKey } );
		}
	#endregion

	#region JavaScript
		private JsBinder JsBinder = null!;

		public JSRuntime JsRuntime
		{
			get { return Get( () => JsRuntime, (JSRuntime)null! ); }
			set { Set( () => JsRuntime, value ); }
		}

		[DependsUpon( nameof( JsRuntime ) )]
		public void WhenJsRuntimeChanges()
		{
			JsBinder = new JsBinder( JsRuntime );
		}
	#endregion
	}
}