﻿#nullable enable

using System;
using System.Threading.Tasks;
using ViewModelsBase;

namespace ViewModels.ShippingPortal
{
	public class ShippingPortalViewModel : ViewModelBase
	{
		public ShippingPortalViewModel( Func<Task<string>>? onLoadData, Func<string, Task>? onSaveData, Action? osStateChanged ) : base( onLoadData, onSaveData, osStateChanged )
		{
		}
	}
}