﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzureRemoteService;
using Protocol.Data;
using Utils;
using ViewModelsBase;

namespace ViewModels.Ids
{
	public static class LogExtensions
	{
		private static readonly TimeSpan ZeroHour = TimeSpan.FromHours( 0 );

		public static List<SelectableItem> ToLogList( this WebLogList logList, Func<string, Task> contentFunc )
		{
			var List = new List<SelectableItem>();

			List.AddRange( from J in logList
			               orderby J descending
			               select new SelectableItem( List, J, async job => { await contentFunc( job ); } ) );

			return List;
		}

		public static DateTimeOffset ForceUtc( this DateTimeOffset date ) => new( date.DateTime, ZeroHour );
	}

	public class SelectableItem
	{
		private const string SELECTED   = "selected",
		                     UNSELECTED = "";

		public string Caption { get; }

		public bool Selected
		{
			get => _Selected;
			set
			{
				foreach( var Item in Owner )
				{
					if( Item != this )
					{
						Item._Selected         = false;
						Item.SelectedAttribute = UNSELECTED;
					}
				}

				_Selected = value;

				if( value )
				{
					SelectedAttribute = SELECTED;
					OnSelected( Caption );
				}
				else
					SelectedAttribute = UNSELECTED;
			}
		}

		public string SelectedAttribute { get; private set; } = UNSELECTED;

		private readonly List<SelectableItem> Owner;

		private bool _Selected;

		private readonly Func<string, Task> OnSelected;

		public SelectableItem( List<SelectableItem> owner, string caption, Func<string, Task> onSelected )
		{
			Owner      = owner;
			Caption    = caption;
			OnSelected = onSelected;
		}
	}

	public class LogsViewModel : ViewModelBase
	{
		public string LogContents
		{
			get { return Get( () => LogContents, "" ); }
			set { Set( () => LogContents, value ); }
		}

		private bool InLogChange;

		public LogsViewModel( Action refresh ) : base( refresh )
		{
		}

		private async Task DoContents( string jobName, Func<string, string, string, Task<string>> contentFunc, Func<string, string>? format = null )
		{
			Busy = true;

			try
			{
				var Temp = await contentFunc( CarrierId, LogType, jobName );
				LogContents = format is null ? Temp : format( Temp );
			}
			finally
			{
				Busy = false;
			}
		}

	#region Busy
		public bool Busy
		{
			get { return Get( () => Busy, false ); }
			set { Set( () => Busy, value ); }
		}


		public string BusyVisibility
		{
			get { return Get( () => BusyVisibility, "hidden" ); }
			set { Set( () => BusyVisibility, value ); }
		}

		private bool BusyWasVisible;

		[DependsUpon( nameof( Busy ), Delay = 1500 )]
		public void WhenBusyChanges()
		{
			if( Busy ) // Still busy
			{
				BusyWasVisible = true;
				BusyVisibility = "visible";
			}
		}

		[DependsUpon( nameof( Busy ) )]
		public void WhenBusyChangesToFalse()
		{
			if( BusyWasVisible )
			{
				BusyWasVisible = false;
				BusyVisibility = "hidden";
			}
		}
	#endregion

	#region Logs
		public List<SelectableItem> LogList
		{
			get { return Get( () => LogList, new List<SelectableItem>() ); }
			set { Set( () => LogList, value ); }
		}

		private async Task DoLogList( string logType, Func<string, string, Task<WebLogList>> logFunc, Func<string, Task> contentFunc )
		{
			LogType = logType;
			LogList = ( await logFunc( CarrierId, logType ) ).ToLogList( contentFunc );
		}
	#endregion

	#region LogTypes
		private string LogType = "";


		public List<SelectableItem> LogTypesList
		{
			get { return Get( () => LogTypesList, new List<SelectableItem>() ); }
			set { Set( () => LogTypesList, value ); }
		}

		private async Task DoLogTypeList( string carrierId, Func<string, Task<WebLogList>> listFunc, Func<string, Task> logFunc )
		{
			CarrierId = carrierId;

			LogList     = new List<SelectableItem>();
			LogContents = "";

			var List = new List<SelectableItem>();

			List.AddRange( from J in await listFunc( carrierId )
			               select new SelectableItem( List, J, async logType => { await logFunc( logType ); } ) );

			LogTypesList = List;
		}
	#endregion

	#region Carrier
		private string CarrierId = "";

		public List<SelectableItem> CarrierList
		{
			get { return Get( () => CarrierList, new List<SelectableItem>() ); }
			set { Set( () => CarrierList, value ); }
		}

		private async void DoCarrierList( Func<Task<WebLogCarrierList>> func, Func<string, Task> typeFunc )
		{
			LogTypesList = new List<SelectableItem>();
			LogList      = new List<SelectableItem>();
			LogContents  = "";

			var CList = new List<SelectableItem>();

			CList.AddRange( from C in await func()
			                select new SelectableItem( CList, C, async carrierId => { await typeFunc( carrierId ); } ) );

			CarrierList = CList;
		}
	#endregion

	#region SOAP
		public bool SoapLogs
		{
			get { return Get( () => SoapLogs, false ); }
			set { Set( () => SoapLogs, value ); }
		}

		[DependsUpon( nameof( SoapLogs ) )]
		public void WhenSoapLogsChange()
		{
			if( !InLogChange )
			{
				InLogChange = true;
				WebJobs     = false;

				if( SoapLogs )
					DoCarrierList( Azure.Client.RequestGetSOAPCarrierList, OnGetSoapTypes );

				InLogChange = false;
			}
		}

		private async Task OnGetSoapTypes( string carrierId )
		{
			await DoLogTypeList( carrierId, Azure.Client.RequestGetSOAPLogList, OnGetSOAPLogs );
		}

		public async Task OnGetSOAPLogs( string logType )
		{
			await DoLogList( logType, Azure.Client.RequestGetSOAPLogs, OnGetSOAPContents );
		}

		public async Task OnGetSOAPContents( string logName )
		{
			await DoContents( logName, Azure.Client.RequestGetSOAPLogContents, contents =>
			                                                                   {
				                                                                   var Temp = contents;
				                                                                   return Temp;
			                                                                   } );
		}

		public void SoapLogsClick()
		{
			SoapLogs = true;
		}
	#endregion

	#region Web Job
		public bool WebJobs
		{
			get { return Get( () => WebJobs, false ); }
			set { Set( () => WebJobs, value ); }
		}

		[DependsUpon( nameof( WebJobs ) )]
		public void WhenWebJobsChange()
		{
			if( !InLogChange )
			{
				InLogChange = true;
				SoapLogs    = false;

				if( WebJobs )
					DoCarrierList( Azure.Client.RequestGetWebJobCarrierList, OnGetJobTypes );

				InLogChange = false;
			}
		}

		public void WebJobsClick()
		{
			WebJobs = true;
		}

		private async Task OnGetJobTypes( string carrierId )
		{
			await DoLogTypeList( carrierId, Azure.Client.RequestGetWebJobList, OnGetJobs );
		}

		public async Task OnGetJobs( string jobType )
		{
			await DoLogList( jobType, Azure.Client.RequestGetWebJobs, OnGetJobContents );
		}

		public async Task OnGetJobContents( string jobName )
		{
			await DoContents( jobName, Azure.Client.RequestGetWebJobContents );
		}
	#endregion

	#region Search
		[DependsUpon( nameof( WebJobs ) )]
		[DependsUpon( nameof( SoapLogs ) )]
		[DependsUpon( nameof( FromDate ) )]
		[DependsUpon( nameof( ToDate ) )]
		[DependsUpon( nameof( LogTypesList ) )]
		[DependsUpon( nameof( CarrierList ) )]
		public void EnableSearch()
		{
			DisableSearchAsString = ( ( SoapLogs || WebJobs ) && ( CarrierList.Count > 0 ) && ( LogTypesList.Count > 0 ) && ( FromDate <= ToDate ) ).ToEnabled();
		}


		public DateTimeOffset FromDate
		{
			get { return Get( () => FromDate, DateTimeOffset.Now.Date ); }
			set { Set( () => FromDate, value ); }
		}


		public bool Utc
		{
			get { return Get( () => Utc, false ); }
			set { Set( () => Utc, value ); }
		}


		public DateTimeOffset ToDate
		{
			get { return Get( () => ToDate, DateTimeOffset.Now.EndOfDay() ); }
			set { Set( () => ToDate, value ); }
		}


        public string SearchString
        {
            get { return Get(() => SearchString, ""); }
            set { Set(() => SearchString, value); }
        }


        public string DisableSearchAsString
		{
			get { return Get( () => DisableSearchAsString, Extensions.TRUE ); }
			set { Set( () => DisableSearchAsString, value ); }
		}

		public async Task OnSearch()
		{
			Busy        = true;
			InLogChange = true;

			var IsUtc = Utc;

			try
			{
				var Search = new WebLogSearch
				             {
					             CarrierId    = CarrierId,
					             LogType      = LogType,
					             FromDateTime = IsUtc ? FromDate.ForceUtc() : FromDate.LocalDateTime,
					             ToDateTime   = IsUtc ? ToDate.ForceUtc() : ToDate.LocalDateTime,
					             SearchString = SearchString
				             };

				if( WebJobs )
					LogList = ( await Azure.Client.RequestSearchWebJobLogs( Search ) ).ToLogList( OnGetJobContents );
				else if( SoapLogs )
					LogList = ( await Azure.Client.RequestSearchSOAPLogs( Search ) ).ToLogList( OnGetSOAPContents );
			}
			finally
			{
				InLogChange = false;
				Busy        = false;
			}
		}
	#endregion
	}
}