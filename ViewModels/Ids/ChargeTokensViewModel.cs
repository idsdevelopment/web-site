﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzureRemoteService;
using Protocol.Data;
using Utils;
using ViewModelsBase;

namespace ViewModels.Ids
{
	public class DisplayCarrierChargeToken
	{
		public string Id { get; }

		public string CarrierId
		{
			get => _CarrierId;
			set
			{
				_CarrierId = value.ToUpper().ToAlphaNumeric();
				OnChange();
			}
		}

		public decimal SingleTokenRate
		{
			get => _SingleTokenRate;
			set
			{
				_SingleTokenRate = value;
				OnChange();
			}
		}


		public int ExpiresInDays { get; set; }


		public bool Deleted
		{
			get => _Deleted;
			set
			{
				_Deleted = value;
				OnChange();
			}
		}

		public string ExpandedVisible => Collapsed.ToVisibility();

		public bool Expanded
		{
			get => _Expanded;
			set
			{
				if( !Expanding )
				{
					Expanding = true;
					_Expanded = value;
					Collapsed = !value;
					Expanding = false;
				}
			}
		}


		public string CollapsedVisible => Expanded.ToVisibility();

		public bool Collapsed
		{
			get => _Collapsed;
			set
			{
				if( !Expanding )
				{
					Expanding  = true;
					_Collapsed = value;
					Expanded   = !value;
					Expanding  = false;
				}
			}
		}

		public string DeleteVisible => IsNew.ToVisibility();

		public bool IsNew { get; }


		public bool IsValid => CarrierId.IsNotNullOrWhiteSpace() && ( SingleTokenRate > 0 );

		private bool Expanding,
		             _Expanded,
		             _Collapsed = true,
		             _Deleted;

		private string _CarrierId = "";


		private decimal _SingleTokenRate;


		private readonly Action<string>? _OnChange;

		public DisplayCarrierChargeToken( Action<string> onChange, string id, decimal defaultRate, int expiresInDays, bool isNew = false )
		{
			Id              = id;
			SingleTokenRate = defaultRate;
			ExpiresInDays   = expiresInDays;

			IsNew     = isNew;
			_OnChange = onChange;
		}

		public DisplayCarrierChargeToken( DisplayCarrierChargeToken t, Action<string> onChange )
		{
			Id              = t.Id;
			CarrierId       = t.CarrierId;
			SingleTokenRate = t.SingleTokenRate;
			ExpiresInDays   = t.ExpiresInDays;

			_OnChange = onChange;
		}

		private void OnChange()
		{
			_OnChange?.Invoke( CarrierId );
		}
	}

	public class ChargeTokensViewModel : ViewModelBase
	{
		private const string FALSE = "false",
		                     TRUE  = "true";


		public List<DisplayCarrierChargeToken> CarrierChargeTokens
		{
			get { return Get( () => CarrierChargeTokens, new List<DisplayCarrierChargeToken>() ); }
			set { Set( () => CarrierChargeTokens, value ); }
		}

		private bool Loading = true;

		private readonly List<DisplayCarrierChargeToken> SaveCarrierChargeTokens = new();

		private          decimal SaveDefaultValue;
		private readonly Random  Rand = new();

		protected override async void OnInitialised()
		{
			base.OnInitialised();
			await ReLoad();
		}

		public ChargeTokensViewModel( Action refresh ) : base( refresh )
		{
		}

		private string NewId() => $"Id{Rand.Next():000000}";

		private async Task ReLoad()
		{
			var Tokens = await Azure.Client.RequestGetCarrierChargeTokens( new GetChargeTokens() );

			DefaultTokenValue      = Tokens.DefaultTokenValue;
			DefaultTokenExpiryDays = Tokens.DefaultTokenExpiresInDays;

			CarrierChargeTokens = ( from T in Tokens.CarrierChargeTokens
			                        orderby T.CarrierId
			                        select new DisplayCarrierChargeToken( OnCarrierChargeTokensChange, NewId(), T.SingleTokenRate, T.ExpiresInDays )
			                               {
				                               CarrierId = T.CarrierId
			                               }
			                      ).ToList();
			UpdateUndo();
			DefaultButtonState();
		}

		private void DefaultButtonState()
		{
			DisableUndo  = TRUE;
			DisableApply = TRUE;
			DisableAdd   = FALSE;
		}

		private void UpdateUndo()
		{
			SaveDefaultValue = DefaultTokenValue;
			SaveCarrierChargeTokens.Clear();

			SaveCarrierChargeTokens.AddRange( from Token in CarrierChargeTokens
			                                  select new DisplayCarrierChargeToken( Token, OnCarrierChargeTokensChange ) );
			DefaultButtonState();
		}

	#region OnChange
		private void OnCarrierChargeTokensChange( string carrierId )
		{
			OnChangeDelayCarrierId = carrierId;
		}


		public string OnChangeDelayCarrierId
		{
			get { return Get( () => OnChangeDelayCarrierId, "" ); }
			set { Set( () => OnChangeDelayCarrierId, value ); }
		}

		[DependsUpon350( nameof( OnChangeDelayCarrierId ) )]
		public void WhenOnChangeDelayChanges()
		{
			if( !Loading )
			{
				if( OnChangeDelayCarrierId.IsNotNullOrWhiteSpace() )
				{
					var Cid = OnChangeDelayCarrierId;
					OnChangeDelayCarrierId = "";

					var Count1 = 0;

					foreach( var Token in CarrierChargeTokens )
					{
						if( ( ( Token.CarrierId == Cid ) && ( ++Count1 > 1 ) ) || !Token.IsValid )
						{
							DisableApply = TRUE;
							DisableAdd   = TRUE;
							return;
						}
					}

					DisableApply = FALSE;
					DisableAdd   = FALSE;
				}
			}
			else
			{
				OnChangeDelayCarrierId = "";
				Loading                = false;
				DisableAdd             = FALSE;
			}
		}
	#endregion

	#region Buttons
		public string DisableUndo
		{
			get { return Get( () => DisableUndo, TRUE ); }
			set { Set( () => DisableUndo, value ); }
		}


		public string DisableApply
		{
			get { return Get( () => DisableApply, TRUE ); }
			set { Set( () => DisableApply, value ); }
		}

		public string DisableAdd
		{
			get { return Get( () => DisableAdd, FALSE ); }
			set { Set( () => DisableAdd, value ); }
		}

	#region Actions
		public void Undo()
		{
			Loading = true;

			CarrierChargeTokens.Clear();

			CarrierChargeTokens.AddRange( from Token in SaveCarrierChargeTokens
			                              select new DisplayCarrierChargeToken( Token, OnCarrierChargeTokensChange ) );
			DefaultButtonState();
			DefaultTokenValue = SaveDefaultValue;
		}

		public void Add()
		{
			CarrierChargeTokens.Add( new DisplayCarrierChargeToken( OnCarrierChargeTokensChange, NewId(), DefaultTokenValue, DefaultTokenExpiryDays, true ) );
			DisableAdd   = TRUE;
			DisableApply = TRUE;
			DisableUndo  = FALSE;
		}

		public async void Apply()
		{
			var Update = new ChargeTokens
			             {
				             DefaultTokenValue         = DefaultTokenValue,
				             DefaultTokenExpiresInDays = DefaultTokenExpiryDays,

				             CarrierChargeTokens = ( from T in CarrierChargeTokens
				                                     where !T.Deleted
				                                     select new CarrierChargeToken
				                                            {
					                                            CarrierId       = T.CarrierId,
					                                            ExpiresInDays   = T.ExpiresInDays,
					                                            SingleTokenRate = T.SingleTokenRate
				                                            } ).ToList()
			             };

			if( await Azure.Client.RequestAddUpdateCarrierChargeTokens( Update ) )
				await ReLoad();
		}
	#endregion
	#endregion

	#region Defaults
		public decimal DefaultTokenValue
		{
			get { return Get( () => DefaultTokenValue, 0 ); }
			set { Set( () => DefaultTokenValue, value ); }
		}

		public int DefaultTokenExpiryDays
		{
			get { return Get( () => DefaultTokenExpiryDays, 0 ); }
			set { Set( () => DefaultTokenExpiryDays, value ); }
		}

		[DependsUpon350( nameof( DefaultTokenValue ) )]
		[DependsUpon350( nameof( DefaultTokenExpiryDays ) )]
		public void WhenDefaultValueChanges()
		{
			if( !Loading )
			{
				DisableApply = FALSE;
				DisableUndo  = FALSE;
			}
		}
	#endregion
	}
}