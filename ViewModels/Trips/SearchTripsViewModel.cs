﻿#nullable enable

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AzureRemoteService;
using Protocol.Data;
using Utils;
using ViewModelsBase;

// ReSharper disable InconsistentNaming

namespace ViewModels.Models.Trips
{
	public class SearchTripsViewModel : ViewModelBase
	{
		public class DisplayTrip : Trip
		{
			public bool Expand
			{
				get => _Expand;
				set
				{
					_Expand        = value;
					ExpandAsString = value.ToString();
				}
			}

			public string ExpandAsString { get; set; } = "true";

			public bool Expandable { get; }

			public string StatusAsString => Status1.AsString();

			public bool ExpandItems
			{
				get => _ExpandItems;
				set
				{
					_ExpandItems        = value;
					ExpandItemsAsString = value.ToString();
				}
			}


			public string ExpandItemsAsString { get; set; } = "true";

			public DisplayTrip( Trip t ) : base( t )
			{
				Expandable = Packages.Count > 0;
			}

			private bool _Expand      = true,
			             _ExpandItems = true;

			public void ToggleExpand()
			{
				Expand = !Expand;
			}

			public void ToggleExpandItems()
			{
				ExpandItems = !ExpandItems;
			}
		}

		public SearchTripsViewModel( Action refresh ) : base( refresh )
		{
		}


		protected override void OnFetchValues()
		{
			base.OnFetchValues();

			Task.WhenAll( Task.Run( async () =>
			                        {
				                        var PTypes = await Azure.Client.RequestGetPackageTypes();

				                        var Temp = ( from P in PTypes
				                                     select P.Description ).ToList();
				                        Temp.Insert( 0, "" );
				                        PackageTypes = Temp;
			                        } ),
			              Task.Run( async () =>
			                        {
				                        var SLevels = await Azure.Client.RequestGetServiceLevels();

				                        var Temp = ( from S in SLevels
				                                     select S ).ToList();
				                        Temp.Insert( 0, "" );
				                        ServiceLevels = Temp;
			                        } )
			            );
		}

	#region Actions
		public void ExecuteSearch()
		{
			DisableSearch = true;

			var Fd                      = FromDate.StartOfDay();
			var Td                      = ToDate.EndOfDay();
			var SearchByClientReference = SearchByReference;
			var ClientReference         = Reference;

			var SelectedCompany           = "";
			var SelectedDelCompanyAddress = "";
			var SelectedPUCompanyAddress  = "";
			var TripId                    = "";
			var SearchByTripId            = false;

			Task.Run( async () =>
			          {
				          try
				          {
					          var Trps = await Azure.Client.RequestSearchTrips( new SearchTrips
					                                                            {
						                                                            FromDate                      = Fd,
						                                                            ToDate                        = Td,
						                                                            TripId                        = TripId,
						                                                            ByReference                   = SearchByClientReference,
						                                                            ByTripId                      = SearchByTripId,
						                                                            Reference                     = ClientReference,
						                                                            StartStatus                   = FromStatus.AsStatus(),
						                                                            EndStatus                     = ToStatus.AsStatus(),
						                                                            CompanyCode                   = SelectedCompany,
						                                                            SelectedCompanyPUAddressName  = SelectedPUCompanyAddress,
						                                                            SelectedCompanyDelAddressName = SelectedDelCompanyAddress,
						                                                            PackageType                   = PackageType,
						                                                            ServiceLevel                  = ServiceLevel
					                                                            } );

					          Trips = ( from T in Trps
					                    select new DisplayTrip( T ) ).ToList();

					          FilterTrips();
					          DisableSearch = false;
				          }
				          catch( Exception Exception )
				          {
					          Console.WriteLine( Exception );
				          }
			          } );
		}
	#endregion

	#region Trips
		private void FilterTrips()
		{
			DisplayTrips = Trips;
		}

		public List<DisplayTrip>? DisplayTrips
		{
			get { return Get( () => DisplayTrips, new List<DisplayTrip>() ); }
			set { Set( () => DisplayTrips, value ); }
		}

		private List<DisplayTrip>? Trips;
	#endregion

	#region Filters
		public List<string> PackageTypes
		{
			get { return Get( () => PackageTypes, new List<string>() ); }
			set { Set( () => PackageTypes, value ); }
		}


		public string PackageType
		{
			get { return Get( () => PackageType, "" ); }
			set { Set( () => PackageType, value ); }
		}


		public string ServiceLevel
		{
			get { return Get( () => ServiceLevel, "" ); }
			set { Set( () => ServiceLevel, value ); }
		}


		public List<string> ServiceLevels
		{
			get { return Get( () => ServiceLevels, new List<string>() ); }
			set { Set( () => ServiceLevels, value ); }
		}


		public List<string> StatusList
		{
			get { return Get( () => StatusList, TripExtensions.StatusAsStringArray[ (int)STATUS.NEW .. (int)STATUS.UNKNOWN ].ToList() ); }
			set { Set( () => StatusList, value ); }
		}


		public string FromStatus
		{
			get { return Get( () => FromStatus, TripExtensions.StatusAsStringArray[ (int)STATUS.NEW ] ); }
			set { Set( () => FromStatus, value ); }
		}


		public string ToStatus
		{
			get { return Get( () => ToStatus, TripExtensions.StatusAsStringArray[ (int)STATUS.VERIFIED ] ); }
			set { Set( () => ToStatus, value ); }
		}

		[DependsUpon( nameof( FromStatus ) )]
		[DependsUpon( nameof( ToStatus ) )]
		public void WhenStatusChanges()
		{
			var FStat = FromStatus;
			var TStat = ToStatus;

			var Fs = FStat.AsStatus();
			var Ts = TStat.AsStatus();

			if( Ts < Fs )
			{
				FromStatus = TStat;
				ToStatus   = FStat;
			}
		}
	#endregion

	#region Toolbar
		public bool DisableSearch
		{
			get { return Get( () => DisableSearch, false ); }
			set { Set( () => DisableSearch, value ); }
		}


		public string DisableSearchAsString
		{
			get { return Get( () => DisableSearchAsString, "false" ); }
			set { Set( () => DisableSearchAsString, value ); }
		}

		[DependsUpon( nameof( DisableSearch ) )]
		public void WhenDisableSearchChanges()
		{
			DisableSearchAsString = DisableSearch.ToString();
		}


		public bool DisableCancel
		{
			get { return Get( () => DisableCancel, true ); }
			set { Set( () => DisableCancel, value ); }
		}


		public string DisableCancelAsString
		{
			get { return Get( () => DisableCancelAsString, "true" ); }
			set { Set( () => DisableCancelAsString, value ); }
		}

		[DependsUpon250( nameof( PackageType ) )]
		[DependsUpon250( nameof( ServiceLevel ) )]
		[DependsUpon250( nameof( ShowAsLocalTime ) )]
		[DependsUpon250( nameof( FromDate ) )]
		[DependsUpon250( nameof( ToDate ) )]
		[DependsUpon250( nameof( FromStatus ) )]
		[DependsUpon250( nameof( ToStatus ) )]
		public void EnableCancel()
		{
			DisableCancel = false;
		}

		[DependsUpon( nameof( DisableCancel ) )]
		public void WhenDisableCancelChanges()
		{
			DisableCancelAsString = DisableCancel.ToString();
		}


		public void OnCancel()
		{
			SetDefaultValues();
		}
	#endregion

	#region Reference / Shipment
		public bool SearchByShipmentId
		{
			get { return Get( () => SearchByShipmentId, false ); }
			set { Set( () => SearchByShipmentId, value ); }
		}


		public string ShipmentId
		{
			get { return Get( () => ShipmentId, "" ); }
			set { Set( () => ShipmentId, value ); }
		}


		public bool SearchByReference
		{
			get { return Get( () => SearchByReference, false ); }
			set { Set( () => SearchByReference, value ); }
		}


		public string Reference
		{
			get { return Get( () => Reference, "" ); }
			set { Set( () => Reference, value ); }
		}


		[DependsUpon( nameof( SearchByReference ) )]
		public void WhenSearchByReferenceChanges()
		{
			var Sbs = SearchByShipmentId;
			var Sbr = SearchByReference;

			DisableDateSearch = Sbs || Sbr;

			if( Sbr )
				SearchByShipmentId = false;
		}


		[DependsUpon( nameof( SearchByShipmentId ) )]
		public void WhenSearchByShipmentIdChanges()
		{
			var Sbs = SearchByShipmentId;
			var Sbr = SearchByReference;

			DisableDateSearch = Sbs || Sbr;

			if( Sbs )
				SearchByReference = false;
		}


		public bool DisableShipmentId
		{
			get { return Get( () => DisableShipmentId, false ); }
			set { Set( () => DisableShipmentId, value ); }
		}


		public string DisableShipmentIdAsString
		{
			get { return Get( () => DisableShipmentIdAsString, "false" ); }
			set { Set( () => DisableShipmentIdAsString, value ); }
		}

		[DependsUpon( nameof( DisableShipmentId ) )]
		public void WhenDisableShipmentIdChanges()
		{
			DisableShipmentIdAsString = DisableShipmentId.ToString();
		}


		public bool DisableReference
		{
			get { return Get( () => DisableReference, false ); }
			set { Set( () => DisableReference, value ); }
		}


		public string DisableReferenceAsString
		{
			get { return Get( () => DisableReferenceAsString, "false" ); }
			set { Set( () => DisableReferenceAsString, value ); }
		}

		[DependsUpon( nameof( DisableReference ) )]
		public void WhenDisableReferenceChanges()
		{
			DisableReferenceAsString = DisableReference.ToString();
		}
	#endregion

	#region DateSearch
		public bool ShowAsLocalTime
		{
			get { return Get( () => ShowAsLocalTime, false ); }
			set { Set( () => ShowAsLocalTime, value ); }
		}


		public bool DisableDateSearch
		{
			get { return Get( () => DisableDateSearch, false ); }
			set { Set( () => DisableDateSearch, value ); }
		}

		public string DisableDateSearchAsString
		{
			get { return Get( () => DisableDateSearchAsString, "false" ); }
			set { Set( () => DisableDateSearchAsString, value ); }
		}

		public DateTimeOffset FromDate
		{
			get { return Get( () => FromDate, DateTimeOffset.Now ); }
			set { Set( () => FromDate, value ); }
		}


		public DateTimeOffset ToDate
		{
			get { return Get( () => ToDate, DateTimeOffset.Now ); }
			set { Set( () => ToDate, value ); }
		}

		[DependsUpon( nameof( DisableDateSearch ) )]
		public void WhenDisableDateSearchChanges()
		{
			DisableDateSearchAsString = DisableDateSearch.ToString();
		}

		[DependsUpon( nameof( FromDate ) )]
		[DependsUpon( nameof( ToDate ) )]
		public void WhenDatesChange()
		{
			var FDate = FromDate;
			var TDate = ToDate;

			if( TDate < FDate )
			{
				FromDate = TDate;
				ToDate   = FDate;
			}
		}
	#endregion
	}
}