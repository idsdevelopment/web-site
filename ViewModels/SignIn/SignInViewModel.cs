﻿#nullable enable

using System;
using System.Threading.Tasks;
using AzureRemoteService;
using Globals;
using ViewModels.Models.MainMenu;
using ViewModelsBase;
using Version = Globals.Version;

namespace ViewModels.Models.SignIn
{
	public class SignInViewModel : ViewModelBase
	{
		public enum SIGN_TYPE
		{
			NONE,
			CARRIER,
			DRIVER,
			PORTAL
		}


		[Setting]
		public string CarrierId
		{
			get { return Get( () => CarrierId, "" ); }
			set { Set( () => CarrierId, value ); }
		}


		[Setting]
		public string UserName
		{
			get { return Get( () => UserName, "" ); }
			set { Set( () => UserName, value ); }
		}


		public string Password
		{
			get { return Get( () => Password, "" ); }
			set { Set( () => Password, value ); }
		}

		private SIGN_TYPE SignType
		{
			get => _SignType;
			init
			{
				_SignType = value;
				var Nav = NavMenuViewModel.Instance;
				Nav.SignInType = value;
			}
		}

		private readonly SIGN_TYPE _SignType = SIGN_TYPE.NONE;

		public Action<bool>? OnSignedIn;

		protected override void OnInitialised()
		{
			base.OnInitialised();
			LoadSettings();
		}

		public SignInViewModel( SIGN_TYPE signType, Func<Task<string>>? onLoadData, Func<string, Task>? onSaveData, Action? osStateChanged ) : base( onLoadData, onSaveData, osStateChanged )
		{
			SignType = signType;
		}


	#region Sign In
		public bool DisableSignIn
		{
			get { return Get( () => DisableSignIn, true ); }
			set { Set( () => DisableSignIn, value ); }
		}


		[DependsUpon150( nameof( CarrierId ) )]
		[DependsUpon( nameof( UserName ) )]
		[DependsUpon( nameof( Password ) )]
		public void DoEnable()
		{
			DisableSignIn = ( CarrierId == "" ) || ( UserName == "" ) || ( Password == "" );
			ErrorVisible  = false;
		}

		public async void ExecuteSignIn()
		{
			DisableInput = true;

			try
			{
			#if !DEBUG
				Azure.Slot = Azure.SLOT.BETA;
			#else
				Azure.Slot = Azure.SLOT.ALPHA_TRW;
			#endif
				var Result = await Azure.LogIn( Version.APP_VERSION, CarrierId, UserName, Password, SignType == SIGN_TYPE.DRIVER );

				if( Result == Azure.AZURE_CONNECTION_STATUS.OK )
				{
					Users.CarrierId = CarrierId;
					Users.UserId    = UserName;

					ErrorVisible = false;
					SaveSettings();

					bool Ok;

					var IsIds = await Azure.Client.RequestIsIds();

					if( IsIds )
					{
						var Parts = CarrierId.Split( '/', '\\', StringSplitOptions.RemoveEmptyEntries | StringSplitOptions.TrimEntries );

						Ok = ( Parts.Length == 2 ) && ( string.Compare( Parts[ 1 ], "idsroute", StringComparison.OrdinalIgnoreCase ) == 0 );
					}
					else
						Ok = true;

					if( Ok )
					{
						OnSignedIn?.Invoke( ( SignType == SIGN_TYPE.CARRIER ) && IsIds );
						return;
					}
				}
			}
			catch( Exception E )
			{
				Console.WriteLine( E );
			}
			DisableInput = false;
			ErrorVisible = true;
		}
	#endregion

	#region Disable Input
		public bool DisableInput
		{
			get { return Get( () => DisableInput, false ); }
			set { Set( () => DisableInput, value ); }
		}


		public string DisableInputAsString
		{
			get { return Get( () => DisableInputAsString, "false" ); }
			set { Set( () => DisableInputAsString, value ); }
		}


		[DependsUpon( nameof( DisableInput ) )]
		public void WenDisableInputChanges()
		{
			DisableInputAsString = DisableInput.ToString();
		}
	#endregion

	#region Error
		public bool ErrorVisible
		{
			get { return Get( () => ErrorVisible, false ); }
			set { Set( () => ErrorVisible, value ); }
		}


		public string ErrorHiddenAsString
		{
			get { return Get( () => ErrorHiddenAsString, "true" ); }
			set { Set( () => ErrorHiddenAsString, value ); }
		}

		[DependsUpon( nameof( ErrorVisible ) )]
		public void WhenErrorVisibleChanges()
		{
			ErrorHiddenAsString = ( !ErrorVisible ).ToString();
		}
	#endregion
	}
}