﻿#nullable enable

using System;
using Globals;
using ViewModelsBase;

namespace ViewModels.Models.MainMenu
{
	public class MainViewModel : ViewModelBase
	{
		public const string NOT_SIGNED_IN = "Not Signed In";

		public string UserName
		{
			get { return Get( () => UserName, NOT_SIGNED_IN ); }
			set { Set( () => UserName, value ); }
		}

	#region Enables
		public string EnablePreferencesAsString
		{
			get { return Get( () => EnablePreferencesAsString, "false" ); }
			set { Set( () => EnablePreferencesAsString, value ); }
		}
	#endregion

		public MainViewModel( Action refresh ) : base( refresh )
		{
		}

	#region Carrier Id
		public string CarrierId
		{
			get { return Get( () => CarrierId, "" ); }
			set { Set( () => CarrierId, value ); }
		}

		[DependsUpon( nameof( CarrierId ) )]
		[DependsUpon( nameof( UserName ) )]
		public void WhenIdChanges()
		{
			Users.CarrierId = CarrierId;
			Users.UserId    = UserName;
		}
	#endregion
	}
}