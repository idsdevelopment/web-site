﻿#nullable enable

using System;
using Globals.Settings;
using ViewModelsBase;
using static ViewModels.Models.SignIn.SignInViewModel;

namespace ViewModels.Models.MainMenu
{
	public class NavMenuViewModel : ViewModelBase
	{
		private const string FALSE = "false",
		                     TRUE  = "true";

		public static NavMenuViewModel Instance { get; private set; } = null!;

	#region Priority
		public string EnableDriverEstimate
		{
			get { return Get( () => EnableDriverEstimate, FALSE ); }
			set { Set( () => EnableDriverEstimate, value ); }
		}
	#endregion

		public string EnableFileMenu
		{
			get { return Get( () => EnableFileMenu, FALSE ); }
			set { Set( () => EnableFileMenu, value ); }
		}

		public string EnablePreferences
		{
			get { return Get( () => EnablePreferences, FALSE ); }
			set { Set( () => EnablePreferences, value ); }
		}

		public string EnableSearchTrips
		{
			get { return Get( () => EnableSearchTrips, FALSE ); }
			set { Set( () => EnableSearchTrips, value ); }
		}


		public string EnableMaintainStaff
		{
			get { return Get( () => EnableMaintainStaff, FALSE ); }
			set { Set( () => EnableMaintainStaff, value ); }
		}

		public NavMenuViewModel( Action refresh ) : base( refresh )
		{
			Instance = this;
		}

	#region IDS
		public bool IsIdsSpecialLogin
		{
			get { return Get( () => IsIdsSpecialLogin, false ); }
			set { Set( () => IsIdsSpecialLogin, value ); }
		}

		public string IsIdsSpecialLoginAsString
		{
			get { return Get( () => IsIdsSpecialLoginAsString, FALSE ); }
			set { Set( () => IsIdsSpecialLoginAsString, value ); }
		}
	#endregion

	#region Sign In
		public SIGN_TYPE SignInType
		{
			get { return Get( () => SignInType, SIGN_TYPE.NONE ); }
			set { Set( () => SignInType, value ); }
		}

		public bool SignedIn
		{
			get { return Get( () => SignedIn, false ); }
			set { Set( () => SignedIn, value ); }
		}

		public string SignedInAsString
		{
			get { return Get( () => SignedInAsString, FALSE ); }
			set { Set( () => SignedInAsString, value ); }
		}


		public string NotSignedInAsString
		{
			get { return Get( () => NotSignedInAsString, TRUE ); }
			set { Set( () => NotSignedInAsString, value ); }
		}

		[DependsUpon( nameof( SignedIn ) )]
		public async void WhenSignedInChanged()
		{
			EnableFileMenu       = FALSE;
			EnablePreferences    = FALSE;
			EnableSearchTrips    = FALSE;
			EnableMaintainStaff  = FALSE;
			EnableDriverEstimate = FALSE;

			if( SignedIn )
			{
				var IsIds = IsIdsSpecialLogin;
				var IsPriority = !IsIds && ( await Menu.Preferences() ).IsPriority;

				switch( SignInType )
				{
				case SIGN_TYPE.CARRIER when IsIds:
					IsIdsSpecialLoginAsString = TRUE;
					break;

				case SIGN_TYPE.DRIVER when IsPriority:
					EnableDriverEstimate = TRUE;
					break;

				case SIGN_TYPE.PORTAL:
					break;

				default:
					return;
				}

				// Enables Sign Out
				SignedInAsString    = TRUE;
				NotSignedInAsString = FALSE;
			}
			else
			{
				SignedInAsString    = FALSE;
				NotSignedInAsString = TRUE;
			}
		}
	#endregion
	}
}